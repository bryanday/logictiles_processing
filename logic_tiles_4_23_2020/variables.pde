int segmentSize = 33; //size of segment in pixels 41, 30
int[] perimeter = new int[1];
int perimeterCounter;
int rowCount = 9; //Number of rows and columns 7, 9
int topCount = ceil((float)rowCount/2);
float topSize = ceil((float)rowCount/2)*segmentSize*3.25;
boolean bitChange = false;
PVector offset; 
PVector currentPosition = new PVector(0, 0);
PVector nextPosition;
int gateCounter = 0;
int bitCounter = 0;
PVector positionChange;
PVector hexDiameter;
boolean upDirection = true;
boolean longVect = false;
TilePointer tilePointer;
GateIndicator[] gateIndicator;
BitIndicator[] bitIndicator;
String[] tileTypes = {"and", "or", "xor", "xnor", "nor", "nand"};
