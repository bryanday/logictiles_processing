void mouseWheel(MouseEvent event) {

  float e = event.getCount();
  tilePointer.rotation = ((e/3)*(PI));
  tilePointer.direction+=e;
  if (tilePointer.direction>=6)tilePointer.direction-=6;
  if (tilePointer.direction<=-1)tilePointer.direction+=6;
  //println(tilePointer.direction);
}

void keyPressed() {
  switch(keyCode) {
  case RIGHT:

    tilePointer.rotation = (((float)1/3)*(PI));
    print(tilePointer.rotation);
    tilePointer.direction+=1;
    if (tilePointer.direction>=6)tilePointer.direction-=6;
    if (tilePointer.direction<=-1)tilePointer.direction+=6;
    break;
  case LEFT:
    tilePointer.rotation = (-((float)1/3)*(PI));
    tilePointer.direction-=1;
    if (tilePointer.direction>=6)tilePointer.direction-=6;
    if (tilePointer.direction<=-1)tilePointer.direction+=6;
    break;
  }
  switch(key) {
  case 'r': 
    randomizePerimeter();
    break;
  case ' ': 
    bitChange=!bitChange;
    break;
  }
}

void mouseClicked() {
  if (!bitChange) {
    if (mouseButton == LEFT) {
      for (GateIndicator g : gateIndicator) {
        if (PVector.dist(g.centerPoint, new PVector(mouseX, mouseY))<(positionChange.mag()/2)-10) {

          if ( g.value==-1 && ((g.myDirection && (tilePointer.direction == 0 ||tilePointer.direction == 2||tilePointer.direction == 4))||(!g.myDirection && (tilePointer.direction == 1 ||tilePointer.direction == 3||tilePointer.direction == 5)))) {

            switch(tilePointer.direction) {
            case 0:
            case 5:
              if (bitIndicator[g.connections[0]].value==-1) {
                g.pointingTowards = tilePointer.direction;
                g.value=tilePointer.value;
                g.outConnection = g.connections[0];
                g.inConnection[0] = g.connections[1];
                g.inConnection[1] = g.connections[2];
                //println(g.outConnection, g.inConnection[0], g.inConnection[1]);
                bitIndicator[g.connections[0]].highlight=true;
              }
              break;
            case 4:

            case 1:
              if (bitIndicator[g.connections[1]].value==-1) {
                g.pointingTowards = tilePointer.direction;
                g.value=tilePointer.value;
                g.outConnection = g.connections[1];
                g.inConnection[0] = g.connections[2];
                g.inConnection[1] = g.connections[0];
                //println(g.outConnection, g.inConnection[0], g.inConnection[1]);
                bitIndicator[g.connections[1]].highlight=true;
              }
              break;

            case 2:
            case 3:
              if (bitIndicator[g.connections[2]].value==-1) {
                g.pointingTowards = tilePointer.direction;
                g.value=tilePointer.value;
                g.outConnection = g.connections[2];
                g.inConnection[0] = g.connections[0];
                g.inConnection[1] = g.connections[1];
                //println(g.outConnection, g.inConnection[0], g.inConnection[1]);
                bitIndicator[g.connections[2]].highlight=true;
              }
              break;
            }
            //tilePointer.value = int(random(6));
          }
        }
      }
    }
    if (mouseButton == RIGHT) {
      tilePointer.value++; 
      if (tilePointer.value>5)tilePointer.value=0;
    }
  } else {
    for (int a=0; a<perimeterCounter; a++) {
      if (PVector.dist(bitIndicator[perimeter[a]].centerPoint, new PVector(mouseX, mouseY))<(hexDiameter.mag()/2)) {
        if (bitIndicator[perimeter[a]].value==0)bitIndicator[perimeter[a]].value=1;
        else if (bitIndicator[perimeter[a]].value==1)bitIndicator[perimeter[a]].value=0;
      }
    }
  }
}
