/*
to-do
- better text and icon displays for logic gate types
- selection of tiles from side selection bar rather than right clicking
- right clicking switches to bitChange Mode, dropping current tile
- while in bit change mode, right clicking on a tile on board removes it
*/

void settings() {
  fullScreen(P2D);
}


void setup() {

  textAlign(CENTER);
  gateIndicator = new GateIndicator[1];
  bitIndicator = new BitIndicator[1];
  offset = new PVector((width-topSize)/2, 105); 
  tilePointer = new TilePointer();
  nextPosition = new PVector(0, 0); 
  positionChange = calculatePosChg();
  hexDiameter = calculateHexDiam();
  setupBoard();
  determineClosest();
  noCursor();
}


void draw() {

  background(103, 90, 64);
  calculateBits();
  drawBoard();
  if (bitChange) {
    cursor();
    tilePointer.cursorShape.setVisible(false);
  } else {
    noCursor();
    tilePointer.cursorShape.setVisible(true);
  }
 
   tilePointer.place();
}


void calculateBits() {

  for (int a =0; a<gateIndicator.length; a++) {
    if (gateIndicator[a].value!=-1&&bitIndicator[gateIndicator[a].inConnection[0]].value!=-1&&bitIndicator[gateIndicator[a].inConnection[1]].value!=-1) {
      bitIndicator[gateIndicator[a].outConnection].value = calculateLogic(gateIndicator[a].value, bitIndicator[gateIndicator[a].inConnection[0]].value, bitIndicator[gateIndicator[a].inConnection[1]].value);
    }
  }
}


int calculateLogic(int type, int inBitA, int inBitB) {

  boolean inBit1 = false;
  boolean inBit2 = false;
  if (inBitA==1)inBit1=true;
  if (inBitB==1)inBit2=true;

  boolean boolResult = false;
  switch(type) { //0-and, 1-or, 2-xor, 3-xnor, 4-nor, 5-nand
  case 0: //and
    boolResult = inBit1&&inBit2; 
    break;
  case 1: //or
    boolResult = inBit1||inBit2;
    break;
  case 2:
    boolResult = inBit1^inBit2;
    break;
  case 3:
    boolResult = !(inBit1^inBit2);
    break;
  case 4:
    boolResult = !(inBit1||inBit2);
    break;
  case 5:
    boolResult = !(inBit1||inBit2);
    break;
  }
  return boolResult?0:1;
}


void determineClosest() {

  for (GateIndicator a : gateIndicator) {
    int connectionCounter = 0;
    for (BitIndicator b : bitIndicator) {
      if (PVector.dist(a.centerPoint, b.centerPoint)<=positionChange.mag()) { 
        a.connections[connectionCounter]=b.id;
        //println(a.id, b.id);
        connectionCounter++;
      }
    }
  }

  for (BitIndicator a : bitIndicator) {
    int connectionCounter = 0;
    for (GateIndicator b : gateIndicator) {
      if (PVector.dist(a.centerPoint, b.centerPoint)<=positionChange.mag()) { 
        a.connections[connectionCounter]=b.id;
        //println(a.id, b.id);
        connectionCounter++;
      }
    }
  }
}


void drawBoard() {

  for (GateIndicator a : gateIndicator) a.place();
  for (BitIndicator a : bitIndicator) a.place();
}


void setupBoard() {

  int count = rowCount;
  nextPosition = new PVector(0, 0); 
  gateCounter = 0;
  bitCounter = 0;

  for (int a =1; a<=rowCount; a++) {
    for (int b=0; b<count; b++) {

      if (upDirection)drawHexagon(upDirection, nextPosition); //only draw last row with !upDirection
      if (a<=rowCount/2 || ((a>rowCount/2  && a<rowCount)&&b>0&& b<count-1))drawTriangle(upDirection, nextPosition);
      if (upDirection)nextPosition.add(positionChange.x, positionChange.y);
      else nextPosition.add(positionChange.x, -positionChange.y);
      upDirection = !upDirection;
    }
    if ((count & 1)==1)upDirection = !upDirection;
    nextPosition = new PVector(0, 0);
    if (a<=rowCount/2) {
      nextPosition.add(-positionChange.x*a, a*(2*positionChange.y));
      count+=2;
    } else {
      nextPosition.add(positionChange.x*(a-(rowCount-1)), a*(2*positionChange.y));
      count-=2;
    }
  }

  randomInBits();
}


void randomInBits() {

  perimeter = new int[1];
  perimeterCounter=0;

  for (int b=0; b< bitIndicator.length; b++) {
    int blockCounter=0;
    if (bitIndicator[b].id<topCount) {
      bitIndicator[b].value = int(random(2));
      perimeterAdd(b);
    }
    if (bitIndicator[b].id>=bitIndicator.length-topCount) {
      bitIndicator[b].value = int(random(2));
      perimeterAdd(b);
    }

    for (int a=0; a<topCount; a++) {
      blockCounter += (topCount+a);
      if (bitIndicator[b].id == blockCounter) {
        bitIndicator[b].value = int(random(2));
        perimeterAdd(b); 
        if (b>topCount) {
          bitIndicator[b-1].value = int(random(2));
          perimeterAdd(b-1);
        }
      }
    }
    for (int a=int(topCount); a<rowCount; a++) {
      blockCounter +=((rowCount-1)-a+topCount);
      if (bitIndicator[b].id == blockCounter) {
        bitIndicator[b].value = int(random(2));
        perimeterAdd(b); 
        {
          bitIndicator[b-1].value = int(random(2));
          perimeterAdd(b-1);
        }
      }
    }
  }
}


void perimeterAdd(int bitNum) {

  if (perimeterCounter>0)perimeter = expand(perimeter, perimeterCounter+1);
  perimeter[perimeterCounter] = bitNum;
  perimeterCounter++;
}


void randomizePerimeter() {

  for (int p : perimeter) {
    bitIndicator[p].value=int(random(2));
  }
}


void drawTriangle(boolean direction, PVector startPos) {

  if (gateCounter>0)gateIndicator = (GateIndicator[])expand(gateIndicator, gateIndicator.length+1);
  gateIndicator[gateCounter] = new GateIndicator(startPos.x, startPos.y, direction, gateCounter);
  gateCounter++;
}


void drawHexagon(boolean direction, PVector startPos) {

  if (bitCounter>0)bitIndicator = (BitIndicator[])expand(bitIndicator, bitIndicator.length+1);
  bitIndicator[bitCounter] = new BitIndicator(startPos.x, startPos.y, direction, bitCounter);
  bitCounter++;
}


PVector calculatePosChg() {

  PVector distanceToNext = new PVector(0, 0);

  longVect = false; 
  for (int a=0; a<6; a++) {
    float segment = segmentSize;
    if (longVect) segment = 2* segmentSize; 
    float xVert = cos((PI/3)*a) * segment;
    float  yVert = sin((PI/3)*a) * segment;
    PVector newPosition = new PVector(xVert, yVert);
    if (a==2) {
      distanceToNext = currentPosition.copy();
    }
    currentPosition = new PVector(newPosition.x+currentPosition.x, newPosition.y+currentPosition.y);
    longVect = !longVect;
  }
  return distanceToNext;
}


PVector calculateHexDiam() {

  PVector distanceToNext = new PVector(0, 0);

  //draw hexagon
  for (int a=6; a>=0; a--) {
    float xVert = cos((PI/3)*a) * (segmentSize);
    float  yVert = sin((PI/3)*a) * (segmentSize);
    PVector newPosition = new PVector(xVert, yVert);
    if (a==3) {
      distanceToNext = currentPosition.copy();
    }
    currentPosition = new PVector(newPosition.x+currentPosition.x, newPosition.y+currentPosition.y);
  }
  return distanceToNext;
}
