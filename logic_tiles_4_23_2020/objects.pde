class BitIndicator {

  PShape hex;
  PVector myStartPos;
  PVector centerPoint;
  boolean gateTouched = false;
  boolean highlight = false;
  int value=-1;
  boolean myDirection;
  int[] connections  = {-1, -1, -1, -1, -1, -1};
  int id;

  BitIndicator(float myXPos, float myYPos, boolean mydirection, int myID) {
    myStartPos = new PVector(myXPos, myYPos);
    myDirection = mydirection;
    id = myID;
    createHex();
  }

  void createHex() {

    PVector currentPosition = myStartPos.copy();
    hex = createShape();
    hex.beginShape();

    if (myDirection) {

      //draw hexagon
      for (int a=6; a>=0; a--) {
        PVector newPosition = new PVector((cos((PI/3)*a) * segmentSize), (sin((PI/3)*a) * segmentSize));
        hex.vertex(offset.x + currentPosition.x+newPosition.x, offset.y + currentPosition.y+newPosition.y);
        currentPosition = new PVector(newPosition.x+currentPosition.x, newPosition.y+currentPosition.y);
      }
    } else {

      for (int a=0; a<6; a++) {
        PVector newPosition = new PVector((cos((PI/3)*a) * segmentSize), (sin((PI/3)*a) * segmentSize));
        hex.vertex(offset.x + currentPosition.x+newPosition.x, offset.y + currentPosition.y+newPosition.y);
        currentPosition = new PVector(newPosition.x+currentPosition.x, newPosition.y+currentPosition.y);
      }
    }
    hex.strokeWeight(3);
    hex.endShape();

    float centerX = 0;
    float centerY = 0;
    for (int a=0; a<hex.getVertexCount(); a++) {
      centerX += hex.getVertex(a).x;
      centerY += hex.getVertex(a).y;
    }
    centerX=centerX/hex.getVertexCount();
    centerY=centerY/hex.getVertexCount();
    centerPoint = new PVector(centerX, centerY);
  }


  void place() {
    switch (value) {
    case -1:
      hex.setFill(color(193, 180, 154));
      if (highlight)hex.setFill(color(180, 130, 130));
      break;
    case 0: 
      hex.setFill(color(255, 241, 0));
      break; 
    case 1: 
      hex.setFill(color(0, 173, 238));
      break;
    }



    for (int a : connections) {
      if (a!=-1)
        if (gateIndicator[a].hover==id) {
          hex.setFill(color(90, 180, 90));
        }
    }

    if (gateTouched)hex.setFill(color(90, 180, 90));
    shape(hex);
    fill(0);
    //text(id, centerPoint.x, centerPoint.y);
  }
}


class GateIndicator {
  PVector myStartPos;
  PVector centerPoint;
  PShape triangle;
  int value = -1;
  int pointingTowards = -1;
  int id;
  boolean myDirection;
  int hover=-1;
  int[] connections = {-1, -1, -1}; //top, bot left, bot right or top left, top right, bottom
  int[] inConnection = {-1, -1};
  int outConnection = -1;

  GateIndicator(float myXPos, float myYPos, boolean mydirection, int myID) {
    myStartPos = new PVector(myXPos, myYPos);
    id = myID;
    myDirection = mydirection;
    createTriangle();
  }

  void createTriangle() {

    PVector currentPosition = myStartPos.copy();
    triangle = createShape();
    triangle.beginShape();

    if (myDirection) {

      longVect = false; 
      for (int a=0; a<6; a++) {
        float segment = segmentSize;
        if (longVect) segment = 2* segmentSize; 
        PVector newPosition = new PVector((cos((PI/3)*a) * segment), (sin((PI/3)*a) * segment));
        triangle.vertex(offset.x + currentPosition.x+newPosition.x, offset.y + currentPosition.y+newPosition.y);
        currentPosition = new PVector(newPosition.x+currentPosition.x, newPosition.y+currentPosition.y);
        longVect = !longVect;
      }
    } else {

      longVect=false;
      for (int a=6; a>=0; a--) {
        float segment = segmentSize;
        if (longVect) segment = 2* segmentSize; 
        PVector newPosition = new PVector((cos((PI/3)*a) * segment), (sin((PI/3)*a) * segment));
        triangle.vertex(offset.x + currentPosition.x+newPosition.x, offset.y + currentPosition.y+newPosition.y);
        currentPosition = new PVector(newPosition.x+currentPosition.x, newPosition.y+currentPosition.y);
        longVect = !longVect;
      }
    }

    triangle.strokeWeight(3);
    triangle.endShape();

    float centerX = 0;
    float centerY = 0;
    for (int a=0; a<triangle.getVertexCount(); a++) {
      centerX += triangle.getVertex(a).x;
      centerY += triangle.getVertex(a).y;
    }
    centerX=centerX/triangle.getVertexCount();
    centerY=centerY/triangle.getVertexCount();
    centerPoint = new PVector(centerX, centerY);
  }

  void place() {

    if (value==-1)triangle.setFill(color(193, 180, 154));
    else triangle.setFill(color(180, 130, 130));
    if (!bitChange) {
      if (PVector.dist(centerPoint, new PVector(mouseX, mouseY))<(positionChange.mag()/2)-10) {
        if ((myDirection && (tilePointer.direction == 0 ||tilePointer.direction == 2||tilePointer.direction == 4))||(!myDirection && (tilePointer.direction == 1 ||tilePointer.direction == 3||tilePointer.direction == 5))) {
          triangle.setFill(color(90, 180, 90));
          if (myDirection) {
            switch(tilePointer.direction) {
            case 0: 
              hover =connections[0];
              break;
            case 2:
              hover=connections[2];
              break; 
            case 4:
              hover=connections[1];
              break;
            }
          } else {
            switch(tilePointer.direction) {
            case 1:
              hover=connections[1];
              break; 
            case 3: 
              hover=connections[2];
              break; 
            case 5: 
              hover=connections[0];
              break;
            }
          }
        }
      } else hover = -1;
    } else hover = -1;
    shape(triangle);
    fill(0);
    //text(id, centerPoint.x, centerPoint.y);

    if (value!=-1) {
      pushMatrix();
      textSize(20);
      if (pointingTowards==5||pointingTowards==1)translate(centerPoint.x, centerPoint.y-8);
      else translate(centerPoint.x, centerPoint.y);
      rotate (((float)pointingTowards/3)*(PI));
      text(tileTypes[value], 0, 0);
      //draw directional pointers here
      textSize(12);
      popMatrix();
    }
  }
}

class TilePointer {

  float xPos, yPos;
  int direction; //0 - vertical
  float rotation; 
  int value=1;
  PShape cursorShape;

  TilePointer() {

    cursorShape = createShape();
    cursorShape.beginShape();

    //draw hexagon
    currentPosition = new PVector(0, 0);
    cursorShape.fill(color(180));
    for (int a=6; a>=0; a--) {
      float xVert = cos((PI/3)*a) * (segmentSize);
      float  yVert = sin((PI/3)*a) * (segmentSize);
      PVector newPosition = new PVector(xVert, yVert);
      currentPosition = new PVector(newPosition.x+currentPosition.x, newPosition.y+currentPosition.y);
      cursorShape.vertex(currentPosition.x, currentPosition.y);
    }

    //draw triangle
    currentPosition = new PVector(0, 0);
    boolean longVect = false;
    for (int a=0; a<6; a++) {
      float segment = segmentSize;
      if (longVect) segment = 2* segmentSize; 
      float xVert = cos((PI/3)*a) * segment;
      float  yVert = sin((PI/3)*a) * segment;
      PVector newPosition = new PVector(xVert, yVert);
      currentPosition = new PVector(newPosition.x+currentPosition.x, newPosition.y+currentPosition.y);
      cursorShape.vertex(currentPosition.x, currentPosition.y);
      longVect = !longVect;
    }

    cursorShape.strokeWeight(3);
    cursorShape.endShape(CLOSE);
    cursorShape.translate(-segmentSize/2, -(segmentSize+segmentSize/2));
    cursorShape.translate(mouseX, mouseY);
  }

  void place() {
    xPos=mouseX;
    yPos=mouseY;

    cursorShape.translate(-pmouseX, -pmouseY);
    cursorShape.rotate(rotation);
    rotation = 0;
    cursorShape.translate(xPos, yPos);

    if (!bitChange) {
      shape(cursorShape);
      pushMatrix();
      textSize(20);
      translate(xPos, yPos);
      rotate (((float)direction/3)*(PI));
      text(tileTypes[value], 0, 0);
      textSize(12);
      popMatrix();
    }
  }
}
